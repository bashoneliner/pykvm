pykvm
=====

Pykvm is a web application for managing KVM/QEMU virtual machines and host nodes using libvirt's python bindings, guestfs, and some other open source tools. This is my first project using 
version control or to code specifically with the intention of the project maintaining the ability to be easily contributed to by others. This will definitely be the largest python application
to date for myself. So before I start throwing down code I want to state a few goals annd concepts that I am adopting for this project.

Design Philosophy:
	1. MVC will be used as strictly as possible. There should not be ANY mixture of logic and presentation.
	2. Python bindings for libvirt should be used anywhere possible, avoiding the use of passthru functions that use libvirt as if we were in a bash session.
	3. The client side code should only be html and javascript. Nothing to do with the GUI should be done with a server side language.
	4. The server side code should be constructed as restful as possible, returning only JSON to the GUI.
	5. Host nodes should be totally independent of the application and unaware of it. That is to say, when adding a host node we only need a root ssh key for that node and the rest
	   happens on the web server. There should not be any reason to have to install software other than virtualization packages required for KVM/QEMU and libvirt on the host node. Adding a
	   node should be as easy as installing libvirt, libguestfs, libvirtd, and adding root's ssh key + hostname to Pykvm. 
	6. This application is being started out of the need for a more robust system for managing virtualization than what we currently have via bash scripts at my company. With that being said,
	   I have no problem with creating an application that is tailored to our use case. The application may have features, workflow, etc that do not make sense for the general public, but
	   that is fine. There may be a fork that makes Pykvm more generic in the future but for right now I need to get something together that will work for my use case.
	7. In my work with KVM/QEMU it has become apparent that Ubuntu gets the most attention as far as package maintenance goes so I will be sticking with Ubuntu exclusively for host nodes.

Features
	1. Users and Groups with permissions. VM's and content (content explained later) should be owned by users and groups and have permissions set by them.
	2. Templates. Not just for RAM/disk space/cpu's, but templates that allow you to customize the OS. Things a template can do, such as change the ssh keys are called template modules.
	3. Ability to create template modules from the application.
	4. Customize VM's with content. A template module should be able to add files from the host node/storage to a VM, or allow the user to upload a file that will be added to the VM.
	5. Host node and VM groups. Groups can be used to apply permissions to a list of VM's or to apply a template to them. Same goes with host nodes.
	6. Monitoring will be automatically available via Zabbix. There will be some configs available but the user can modify or provide their own. When spinning up a VM you will decide if you
	   want that VM monitored. The template module is responsible for configuring zabbix, I think. Monitoring may be its own thing instead.
Wish me luck!
Mathew Moon aka OneLiner
